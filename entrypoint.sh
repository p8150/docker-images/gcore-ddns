#!/bin/sh -x
# add crontab job to update dns
while true
do
  MY_IP=$(curl https://ifconfig.me)
  GCORE_CURRENT_IP=$(curl --request GET \
  --url https://api.gcore.com/dns/v2/zones/${DOMAIN#*.}/${DOMAIN}/A \
  --header "authorization: APIKey ${GCORE_PERMANENT_API_TOKEN}" \
  --header 'content-type: application/json' | jq '.resource_records[0].content[0]|tostring' -r)
  if [[ ! $MY_IP == $GCORE_CURRENT_IP ]]
  then
    PAYLOAD="{\"resource_records\":[{\"content\":[\"$MY_IP\"],\"enabled\":true}],\"ttl\":${TTL:-3600}}"
    curl --request PUT \
    --url https://api.gcore.com/dns/v2/zones/${DOMAIN#*.}/${DOMAIN}/A \
    --header "authorization: APIKey ${GCORE_PERMANENT_API_TOKEN}" \
    --header 'content-type: application/json' \
    --data $PAYLOAD
  fi
  sleep ${INTERVAL:-3600}
done
