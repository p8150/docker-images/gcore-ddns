FROM alpine:3.17
WORKDIR /workdir
COPY entrypoint.sh /
RUN apk add --no-cache curl jq

ENTRYPOINT ["/entrypoint.sh"]
