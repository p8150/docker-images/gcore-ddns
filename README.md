# gcore-ddns

Small container to update your ddns IP for gcore.com
Note: need to create you domain zone and name first! After you need to delegate your zone to gcore.com name servers. Create A name record which you will use to store IP e.g. myip.yourdomain.link

## Getting started
Create docker container

```compose
version: "3.8"
services:
  gcore-ddns:
    image: registry.gitlab.com/p8150/docker-images/gcore-ddns:latest
    profiles:
      - "gcore-ddns"
    container_name: gcore-ddns
    deploy:
      restart_policy:
        condition: any
      resources:
        limits:
          memory: 64M
    environment:
      DOMAIN: <DEFINE_YOUR_DOMAIN> # e.g. myip.yourdomain.link
      # GCORE_PERMANENT_API_TOKEN:  # Define in .docker-gitops
    env_file: /root/.docker-gitops
```